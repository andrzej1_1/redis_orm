use std::collections::HashMap;

use crate::active_query::{ActiveQuery, ColumnName, Filter, QuerySort, RedisVal};

#[derive(Default)]
pub struct LuaScriptBuilder {}

impl LuaScriptBuilder {
  pub fn new() -> Self {
    Self {}
  }

  pub fn build_all(&self, query: &ActiveQuery) -> String {
    let key: String = query.model_prefix.clone() + ":a:";
    let quoted_key = Self::quote_value(&key);
    let build_result = format!("n=n+1 pks[n]=redis.call('HGETALL',{quoted_key} .. pk)");
    let ret = "pks";

    Self::build(query, &build_result, ret)
  }

  pub fn build_one(&self, query: &ActiveQuery) -> String {
    let key: String = query.model_prefix.clone() + ":a:";
    let quoted_key = Self::quote_value(&key);
    let build_result = format!("do return redis.call('HGETALL',{quoted_key} .. pk) end");
    let ret = "pks";

    Self::build(query, &build_result, ret)
  }

  pub fn build_column(&self, query: &ActiveQuery, column: ColumnName) -> String {
    let key: String = query.model_prefix.clone() + ":a:";
    let quoted_key = Self::quote_value(&key);
    let quoted_col = Self::quote_value(&column);
    let build_result = format!("n=n+1 pks[n]=redis.call('HGET',{quoted_key} .. pk,{quoted_col})");
    let ret = "pks";

    Self::build(query, &build_result, ret)
  }

  pub fn build_count(&self, query: &ActiveQuery) -> String {
    let build_result = "n=n+1".to_string();
    let ret = "n";

    Self::build(query, &build_result, ret)
  }

  pub fn build_sum(&self, query: &ActiveQuery, column: ColumnName) -> String {
    let key: String = query.model_prefix.clone() + ":a:";
    let quoted_key = Self::quote_value(&key);
    let quoted_col = Self::quote_value(&column);
    let build_result = format!("n=n+redis.call('HGET',{quoted_key} .. pk,{quoted_col})");
    let ret = "n";

    Self::build(query, &build_result, ret)
  }

  pub fn build_average(&self, query: &ActiveQuery, column: ColumnName) -> String {
    let key: String = query.model_prefix.clone() + ":a:";
    let quoted_key = Self::quote_value(&key);
    let quoted_col = Self::quote_value(&column);
    let build_result = format!(
      "n=n+1 if v==nil then v=0 end v=v+redis.call('HGET',{quoted_key} .. pk,{quoted_col})"
    );
    let ret = "v/n";

    Self::build(query, &build_result, ret)
  }

  pub fn build_min(&self, query: &ActiveQuery, column: ColumnName) -> String {
    let key: String = query.model_prefix.clone() + ":a:";
    let quoted_key = Self::quote_value(&key);
    let quoted_col = Self::quote_value(&column);
    let build_result =
      format!("n=redis.call('HGET',{quoted_key} .. pk,{quoted_col}) if v==nil or n<v then v=n end");
    let ret = "v";

    Self::build(query, &build_result, ret)
  }

  pub fn build_max(&self, query: &ActiveQuery, column: ColumnName) -> String {
    let key: String = query.model_prefix.clone() + ":a:";
    let quoted_key = Self::quote_value(&key);
    let quoted_col = Self::quote_value(&column);
    let build_result =
      format!("n=redis.call('HGET',{quoted_key} .. pk,{quoted_col}) if v==nil or n>v then v=n end");
    let ret = "v";

    Self::build(query, &build_result, ret)
  }

  fn build(query: &ActiveQuery, build_result: &str, ret: &str) -> String {
    let mut columns: HashMap<ColumnName, String> = HashMap::new();
    let condition = match &query.filter {
      Some(filter) => Self::build_condition(filter, &mut columns),
      None => "true".into(),
    };
    // TODO: After ActiveRecord has db_attributes() we can validate columns
    // and prevent building invalid query.

    let start = query.offset.unwrap_or(0);
    let mut limit_condition = format!("i>{}", start);
    if let Some(limit) = query.limit {
      limit_condition.push_str(format!(" and i<={}", start + limit).as_str());
    }

    let quoted_key = Self::quote_value(&query.model_prefix);
    let mut load_column_values = String::new();
    for (column, alias) in columns {
      let quoted_column = Self::quote_value(&column);
      load_column_values.push_str(
        format!("local {alias}=redis.call('HGET',{quoted_key} .. ':a:' .. pk, {quoted_column})\n")
          .as_str(),
      );
    }

    let mut get_all_pks = format!("local allpks=redis.call('LRANGE',{quoted_key},0,-1)");

    if let Some(order) = &query.order_by {
      let (order_type, order_column) = match order {
        QuerySort::Asc(col) => ("ASC", col),
        QuerySort::Desc(col) => ("DESC", col),
      };
      get_all_pks = format!("
        local allpks=redis.pcall('SORT', {quoted_key}, 'BY', {quoted_key} .. ':a:*->' .. '{order_column}', '{order_type}')
        if allpks['err'] then
          allpks=redis.pcall('SORT', {quoted_key}, 'BY', {quoted_key} .. ':a:*->' .. '{order_column}', '{order_type}', 'ALPHA')
        end"
      );
    }

    format!(
      "
      {get_all_pks}
      local pks={{}}
      local n=0
      local v=nil
      local i=0
      local key={quoted_key}
      for k,pk in ipairs(allpks) do
          {load_column_values}
          if {condition} then
            i=i+1
            if {limit_condition} then
              {build_result}
            end
          end
      end
      return {ret}"
    )
  }

  fn build_condition(filter: &Filter, columns: &mut HashMap<ColumnName, String>) -> String {
    match filter {
      Filter::Condition(col, val) => Self::build_hash_condition(col, val, columns),
      Filter::NotCondition(filter) => Self::build_not_condition(filter, columns),
      Filter::AndCondition(filters) => Self::build_and_condition(filters, columns),
      Filter::OrCondition(filters) => Self::build_or_condition(filters, columns),
      Filter::BetweenCondition(col, val_l, val_r) => {
        Self::build_between_condition(col, val_l, val_r, columns)
      }
      Filter::NotBetweenCondition(col, val_l, val_r) => {
        Self::build_not_between_condition(col, val_l, val_r, columns)
      }
      Filter::InCondition(col, vals) => Self::build_in_condition(col, vals, columns),
      Filter::NotInCondition(col, vals) => Self::build_not_in_condition(col, vals, columns),
      Filter::LessThanCondition(col, val) => Self::build_less_than_condition(col, val, columns),
      Filter::LessOrEqualCondition(col, val) => {
        Self::build_less_or_equal_condition(col, val, columns)
      }
      Filter::GreaterCondition(col, val) => Self::build_greater_condition(col, val, columns),
      Filter::GreaterOrEqualCondition(col, val) => {
        Self::build_greater_or_equal_condition(col, val, columns)
      }
    }
  }

  fn build_hash_condition(
    column: &str,
    value: &RedisVal,
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    match value {
      RedisVal::Numeric(v) => {
        let column_alias = Self::add_column(column, columns);
        let quoted_value = Self::quote_value(&serde_json::to_string(v).unwrap());
        format!("{column_alias}=={quoted_value}")
      }
      RedisVal::String(v) => {
        let column_alias = Self::add_column(column, columns);
        let quoted_value = Self::quote_value(&serde_json::to_string(v).unwrap());
        format!("{column_alias}=={quoted_value}")
      }
      RedisVal::Bool(v) => {
        let column_alias = Self::add_column(column, columns);
        let quoted_value = Self::quote_value(&serde_json::to_string(v).unwrap());
        format!("{column_alias}=={quoted_value}")
      }
      RedisVal::Empty => {
        // Implementation is based on yii2-redis, but something looks smelly here.
        // TODO: Verify if it works as expected.
        // TODO: Maybe we should use serde here as well (because None is null in JSON)?
        let quoted_column = Self::quote_value(column);
        format!("redis.call('HEXISTS',key .. ':a:' .. pk, {quoted_column})==0")
      }
    }
  }

  fn build_and_condition(filters: &[Filter], columns: &mut HashMap<ColumnName, String>) -> String {
    let mut parts: Vec<String> = vec![];
    for filter in filters {
      let condition = Self::build_condition(filter, columns);
      if !condition.is_empty() {
        parts.push(condition);
      }
    }

    let condition = match parts.len() {
      0 => "".into(),
      _ => format!("({})", parts.join(") and (")),
    };

    condition
  }

  fn build_or_condition(filters: &[Filter], columns: &mut HashMap<ColumnName, String>) -> String {
    let mut parts: Vec<String> = vec![];
    for filter in filters {
      let condition = Self::build_condition(filter, columns);
      if !condition.is_empty() {
        parts.push(condition);
      }
    }

    let condition = match parts.len() {
      0 => "".into(),
      _ => format!("({})", parts.join(") or (")),
    };

    condition
  }

  fn build_not_condition(filter: &Filter, columns: &mut HashMap<ColumnName, String>) -> String {
    let condition = Self::build_condition(filter, columns);
    format!("not ({condition})")
  }

  fn build_between_condition(
    column: &str,
    val_l: &i64,
    val_r: &i64,
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    let column_alias = Self::add_column(column, columns);
    format!("{column_alias} >= {val_l} and {column_alias} <= {val_r}")
  }

  fn build_not_between_condition(
    column: &str,
    val_l: &i64,
    val_r: &i64,
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    let condition = Self::build_between_condition(column, val_l, val_r, columns);
    format!("not ({condition})")
  }

  fn build_in_condition(
    column: &str,
    vals: &[RedisVal],
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    let mut parts: Vec<String> = vec![];
    for val in vals {
      let part = match val {
        RedisVal::Numeric(v) => {
          let column_alias = Self::add_column(column, columns);
          let quoted_value = Self::quote_value(&serde_json::to_string(v).unwrap());
          format!("{column_alias}=={quoted_value}")
        }
        RedisVal::String(v) => {
          let column_alias = Self::add_column(column, columns);
          let quoted_value = Self::quote_value(&serde_json::to_string(v).unwrap());
          format!("{column_alias}=={quoted_value}")
        }
        RedisVal::Bool(v) => {
          let column_alias = Self::add_column(column, columns);
          let quoted_value = Self::quote_value(&serde_json::to_string(v).unwrap());
          format!("{column_alias}=={quoted_value}")
        }
        RedisVal::Empty => {
          let quoted_column = Self::quote_value(column);
          // Implementation is based on yii2-redis, but something looks smelly here.
          // TODO: Verify if it works as expected.
          // TODO: Maybe we should use serde here as well (because None is null in JSON)?
          format!("redis.call('HEXISTS',key .. ':a:' .. pk, {quoted_column})==0")
        }
      };
      parts.push(part);
    }

    let condition = match parts.len() {
      0 => "".into(),
      _ => format!("({})", parts.join(") or (")),
    };

    condition
  }

  fn build_not_in_condition(
    column: &str,
    vals: &[RedisVal],
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    let condition = Self::build_in_condition(column, vals, columns);
    format!("not ({condition})")
  }

  fn build_less_than_condition(
    column: &str,
    val: &i64,
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    let column_alias = Self::add_column(column, columns);
    format!("tonumber({column_alias}) < {val}")
  }

  fn build_less_or_equal_condition(
    column: &str,
    val: &i64,
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    let column_alias = Self::add_column(column, columns);
    format!("tonumber({column_alias}) <= {val}")
  }

  fn build_greater_condition(
    column: &str,
    val: &i64,
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    let column_alias = Self::add_column(column, columns);
    format!("tonumber({column_alias}) > {val}")
  }

  fn build_greater_or_equal_condition(
    column: &str,
    val: &i64,
    columns: &mut HashMap<ColumnName, String>,
  ) -> String {
    let column_alias = Self::add_column(column, columns);
    format!("tonumber({column_alias}) >= {val}")
  }

  fn quote_value(input: &str) -> String {
    let escape_char = |ch| match ch {
      '\0' => Some("\\0"),
      '\n' => Some("\\n"),
      '\r' => Some("\\r"),
      '\\' => Some("\\\\"),
      '\u{1A}' => Some("\\u{1A}"),
      '\'' => Some("\\'"),
      _ => None,
    };

    // Iterate through the characters, checking if each one needs escaping
    let mut escaped_string = String::with_capacity(input.len() + 2);
    escaped_string.push('\'');
    for ch in input.chars() {
      match escape_char(ch) {
        Some(escaped_char) => escaped_string.push_str(escaped_char),
        None => escaped_string.push(ch),
      };
    }
    escaped_string.push('\'');

    escaped_string
  }

  fn add_column(name: &str, columns: &mut HashMap<ColumnName, String>) -> String {
    if let Some(alias) = columns.get(name) {
      return alias.clone();
    }

    let alias = format!(
      "c{}{}",
      name.replace(|c: char| !c.is_ascii_alphabetic(), ""),
      columns.len()
    );
    columns.insert(name.to_string(), alias.clone());

    alias
  }
}
