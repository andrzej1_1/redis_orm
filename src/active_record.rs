use std::collections::BTreeMap;

use async_trait::async_trait;
use redis::AsyncCommands;
use serde::Deserialize;

use crate::active_query::ActiveQuery;
use crate::error::Error;
use crate::stringmap::StringMap;

// TODO: Think about hiding/marking internal methods.
#[async_trait]
pub trait ActiveRecord: StringMap {
  // saves attributes, assigns ID
  async fn db_save(mut self, con: &mut redis::aio::Connection) -> Result<(Self, i64), Error>
  where
    Self: Sized,
  {
    let current_id = self.db_id();
    let (pk, insert) = match current_id {
      Some(v) => (v, false),
      None => (Self::db_get_new_pk(con).await?, true),
    };
    self.db_set_id(Some(pk));

    self.db_before_save(insert)?;

    let attrs = self.to_stringmap()?.into_iter().collect();
    Self::db_save_attributes(con, pk, attrs).await?;

    // After we saved attributes and everything went fine, then we can update pool,
    // but only if this is INSERT operation.
    if insert {
      Self::db_update_pk_pool(con, pk).await?;
    }

    Ok((self, pk))
  }

  // parse attributes (including ID) from Redis
  fn db_parse(attrs: BTreeMap<String, String>) -> Result<(Self, i64), Error>
  where
    for<'de> Self: Deserialize<'de>,
  {
    let model = Self::from_stringmap(attrs)?;
    let pk = model.db_id().ok_or(Error::DeserializationError)?;
    Ok((model, pk))
  }

  // table name prefix
  fn db_prefix() -> String;

  // pk getter
  fn db_id(&self) -> Option<i64>;

  // pk setter
  fn db_set_id(&mut self, id: Option<i64>);

  async fn db_get_last_pk(con: &mut redis::aio::Connection) -> Result<Option<i64>, Error> {
    let key = Self::db_prefix() + ":s:id";
    let val = con.get(key.clone()).await?;
    Ok(val)
  }

  async fn db_get_new_pk(con: &mut redis::aio::Connection) -> Result<i64, Error> {
    let key = Self::db_prefix() + ":s:id";
    let new_val: i64 = con.incr(key.clone(), 1).await?;
    Ok(new_val)
  }

  async fn db_update_pk_pool(con: &mut redis::aio::Connection, pk: i64) -> Result<(), Error> {
    con.rpush(Self::db_prefix(), pk).await?;
    Ok(())
  }

  async fn db_save_attributes(
    con: &mut redis::aio::Connection,
    pk: i64,
    attrs: Vec<(String, String)>,
  ) -> Result<(), Error> {
    let key = Self::db_prefix() + ":a:" + &pk.to_string();
    let _reply: String = con.hset_multiple(key.clone(), &attrs).await?;
    Ok(())
  }

  async fn db_count_all(con: &mut redis::aio::Connection) -> Result<u32, Error> {
    let key = Self::db_prefix();
    let reply: u32 = con.llen(key.clone()).await?;
    Ok(reply)
  }

  fn db_before_save(&self, _insert: bool) -> Result<(), Error> {
    Ok(())
  }

  fn find() -> ActiveQuery {
    ActiveQuery::new(Self::db_prefix())
  }

  // NOTE: This function is necessary to determine dirty attributes.
  // TODO: Auto-implement this function with macro.
  //fn db_attributes() -> HashSet<String>
}
