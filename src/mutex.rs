use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use sha2::Digest;
use sha2::Sha256;

use crate::error::Error;

pub struct MutexGuard {
  name: String,
  lock_key: String,
}

impl MutexGuard {
  pub fn new(name: String) -> Self {
    let lock_key = Self::generate_random_key();
    Self { name, lock_key }
  }

  pub fn name(&self) -> &str {
    self.name.as_str()
  }

  pub fn lock_key(&self) -> &str {
    self.lock_key.as_str()
  }

  fn generate_random_key() -> String {
    let rand_string: String = thread_rng()
      .sample_iter(&Alphanumeric)
      .take(20)
      .map(char::from)
      .collect();

    rand_string
  }
}

#[derive(Default)]
pub struct Mutex {
  // The number of seconds in which the lock will be auto released.
  expire: u32,

  // A string prefixed to every cache key so that it is unique. You may set this property
  // to be an empty string if you don't want to use key prefix. It is recommended that
  // you explicitly set this property to some static value if the cached data needs to be
  // shared among multiple applications.
  key_prefix: String,
}

// Mutex factory component.
impl Mutex {
  pub fn new() -> Self {
    log::debug!("Creating new mutex factory.");
    Self {
      expire: 30,
      key_prefix: String::new(),
    }
  }

  pub async fn lock(
    &self,
    name: String,
    con: &mut redis::aio::Connection,
  ) -> Result<MutexGuard, Error> {
    log::debug!("Trying to lock mutex named '{name}'.");
    let key = self.calculate_key(&name);
    let guard = MutexGuard::new(name);
    let value = guard.lock_key();
    let _res: String = redis::cmd("SET")
      .arg(key)
      .arg(value)
      .arg("NX")
      .arg("PX")
      .arg(self.expire * 1000)
      .query_async(con)
      .await?;

    log::debug!("Mutex locked successfully.");
    Ok(guard)
  }

  pub async fn unlock(
    &self,
    guard: &MutexGuard,
    con: &mut redis::aio::Connection,
  ) -> Result<(), Error> {
    let name = guard.name();
    log::debug!("Trying to unlock mutex named '{name}'.");
    let key = self.calculate_key(name);
    let value = guard.lock_key();
    let script = "
      if redis.call(\"GET\",KEYS[1])==ARGV[1] then
          return redis.call(\"DEL\",KEYS[1])
      else
          return 0
      end"
      .to_string();

    let res: i64 = redis::cmd("EVAL")
      .arg(script)
      .arg("1")
      .arg(key)
      .arg(value)
      .query_async(con)
      .await?;
    if res != 1 {
      log::error!("Unable to unlock mutex.");
      return Err(Error::MutexError("Not possible to unlock.".into()));
    }

    log::debug!("Mutex unlocked successfully.");
    Ok(())
  }

  // Generates a unique key used for storing the mutex in Redis.
  fn calculate_key(&self, name: &str) -> String {
    let name_hash = Sha256::digest(name.as_bytes());
    format!("{}{:x}", self.key_prefix, name_hash)
  }
}

// TODO: Think about automatic unlock on drop().
