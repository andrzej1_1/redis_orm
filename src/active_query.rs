use std::collections::BTreeMap;

use redis::AsyncCommands;
use serde::Deserialize;

use crate::active_record::ActiveRecord;
use crate::error::Error;
use crate::lua_builder::LuaScriptBuilder;

pub enum QuerySort {
  Asc(String),
  Desc(String),
}

pub type ColumnName = String;

pub type NumericVal = i64;
pub type StringVal = String;
pub type BoolVal = bool;

pub enum RedisVal {
  Numeric(NumericVal),
  String(StringVal),
  Bool(BoolVal),
  Empty,
}

pub enum Filter {
  // Simple filter.
  Condition(ColumnName, RedisVal),

  // Complex condition builders.
  NotCondition(Box<Filter>),
  AndCondition(Vec<Filter>),
  OrCondition(Vec<Filter>),
  BetweenCondition(ColumnName, NumericVal, NumericVal),
  NotBetweenCondition(ColumnName, NumericVal, NumericVal),
  InCondition(ColumnName, Vec<RedisVal>),
  NotInCondition(ColumnName, Vec<RedisVal>),
  LessThanCondition(ColumnName, NumericVal),
  LessOrEqualCondition(ColumnName, NumericVal),
  GreaterCondition(ColumnName, NumericVal),
  GreaterOrEqualCondition(ColumnName, NumericVal),
}

pub struct ActiveQuery {
  pub model_prefix: String,
  pub filter: Option<Filter>,
  pub order_by: Option<QuerySort>,
  pub offset: Option<u32>,
  pub limit: Option<u32>,
}

impl ActiveQuery {
  pub fn new(model_prefix: String) -> Self {
    Self {
      model_prefix,
      filter: None,
      order_by: None,
      offset: None,
      limit: None,
    }
  }

  #[must_use]
  pub fn filter(mut self, val: Filter) -> Self {
    self.filter = Some(val);
    self
  }

  #[must_use]
  pub fn order_by(mut self, val: QuerySort) -> Self {
    self.order_by = Some(val);
    self
  }

  #[must_use]
  pub fn offset(mut self, val: u32) -> Self {
    self.offset = Some(val);
    self
  }

  #[must_use]
  pub fn limit(mut self, val: u32) -> Self {
    self.limit = Some(val);
    self
  }

  pub async fn execute_script<T>(
    &self,
    script: String,
    con: &mut redis::aio::Connection,
  ) -> Result<T, Error>
  where
    T: redis::FromRedisValue,
  {
    let result: T = redis::cmd("EVAL")
      .arg(script)
      .arg("0") // no args
      .query_async(con)
      .await?;

    Ok(result)
  }

  pub async fn all<T: ActiveRecord + for<'de> serde::Deserialize<'de>>(
    &self,
    con: &mut redis::aio::Connection,
  ) -> Result<Vec<(T, i64)>, Error> {
    let lua = LuaScriptBuilder::new();
    let script = lua.build_all(self);
    let rows = self
      .execute_script::<Vec<BTreeMap<String, String>>>(script, con)
      .await?;
    let models: Vec<(T, i64)> = rows
      .into_iter()
      .map(T::db_parse)
      .collect::<Result<Vec<(T, i64)>, Error>>()?;
    Ok(models)
  }

  pub async fn one<T: ActiveRecord + for<'de> serde::Deserialize<'de>>(
    &self,
    con: &mut redis::aio::Connection,
  ) -> Result<Option<(T, i64)>, Error> {
    let lua = LuaScriptBuilder::new();
    let script = lua.build_one(self);
    let row = self
      .execute_script::<BTreeMap<String, String>>(script, con)
      .await?;
    if row.is_empty() {
      return Ok(None);
    }
    let model = T::db_parse(row)?;
    Ok(Some(model))
  }

  pub async fn count(&self, con: &mut redis::aio::Connection) -> Result<u32, Error> {
    // Optimization: if no filters then just use LLEN directly.
    if self.filter.is_none() {
      let len: i64 = con.llen(&self.model_prefix).await?;
      return Ok(len as u32);
    }

    let lua = LuaScriptBuilder::new();
    let script = lua.build_count(self);
    let count = self.execute_script::<u32>(script, con).await?;
    Ok(count)
  }

  pub async fn exists(&self, con: &mut redis::aio::Connection) -> Result<bool, Error> {
    let count = self.count(con).await?;
    Ok(count > 0)
  }

  pub async fn column<T>(
    &self,
    column: ColumnName,
    con: &mut redis::aio::Connection,
  ) -> Result<Vec<T>, Error>
  where
    for<'de> T: Deserialize<'de>,
  {
    let lua = LuaScriptBuilder::new();
    let script = lua.build_column(self, column);
    let column = self.execute_script::<Vec<String>>(script, con).await?;
    let column = column
      .into_iter()
      .map(|c| serde_json::from_str::<T>(&c))
      .collect::<Result<Vec<T>, serde_json::error::Error>>()?;
    Ok(column)
  }

  pub async fn sum(
    &self,
    column: ColumnName,
    con: &mut redis::aio::Connection,
  ) -> Result<f32, Error> {
    let lua = LuaScriptBuilder::new();
    let script = lua.build_sum(self, column);
    let sum = self.execute_script::<f32>(script, con).await?;
    Ok(sum)
  }

  pub async fn average(
    &self,
    column: ColumnName,
    con: &mut redis::aio::Connection,
  ) -> Result<f32, Error> {
    let lua = LuaScriptBuilder::new();
    let script = lua.build_average(self, column);
    let average = self.execute_script::<f32>(script, con).await?;
    Ok(average)
  }

  pub async fn min(
    &self,
    column: ColumnName,
    con: &mut redis::aio::Connection,
  ) -> Result<f32, Error> {
    let lua = LuaScriptBuilder::new();
    let script = lua.build_min(self, column);
    let min = self.execute_script::<f32>(script, con).await?;
    Ok(min)
  }

  pub async fn max(
    &self,
    column: ColumnName,
    con: &mut redis::aio::Connection,
  ) -> Result<f32, Error> {
    let lua = LuaScriptBuilder::new();
    let script = lua.build_max(self, column);
    let max = self.execute_script::<f32>(script, con).await?;
    Ok(max)
  }

  // TODO: Implement fn scalar(column, con).
}
