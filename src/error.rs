use thiserror::Error as ThisError;

#[derive(ThisError, Debug)]
pub enum Error {
  #[error("Issue with Redis database.")]
  RedisError(#[from] redis::RedisError),

  #[error("Mutex lock/unlock error.")]
  MutexError(String),

  #[error("Unable to serialize item.")]
  SerializationError,
  #[error("Unable to deserialize item.")]
  DeserializationError,
  #[error("Unable to (de)serialize item.")]
  SerdeError(#[from] serde_json::Error),
}
