use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use crate::error::Error;

pub trait StringMap: Serialize {
  fn to_stringmap(&self) -> Result<BTreeMap<String, String>, Error> {
    let json_val = serde_json::to_value(self)?;
    if let serde_json::Value::Object(o) = json_val {
      let mut map = BTreeMap::new();
      for (attr_name, attr_val) in o.iter() {
        map.insert(attr_name.clone(), attr_val.to_string());
      }
      return Ok(map);
    }

    Err(Error::SerializationError)
  }

  fn from_stringmap(map: BTreeMap<String, String>) -> Result<Self, Error>
  where
    for<'de> Self: Deserialize<'de>,
  {
    let mut json_val = serde_json::Map::new();
    for (attr_name, attr_val_str) in map.iter() {
      let attr_val: serde_json::Value = serde_json::from_str(attr_val_str)?;
      json_val.insert(attr_name.clone(), attr_val);
    }
    let obj: Self = serde_json::from_value(serde_json::Value::Object(json_val))?;

    Ok(obj)
  }
}
