extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::{self, DeriveInput};

#[proc_macro_derive(RedisORM)]
pub fn redis_orm_derive(input: TokenStream) -> TokenStream {
  // Construct a representation of Rust code as a syntax tree
  // that we can manipulate
  let ast = syn::parse_macro_input!(input as DeriveInput);

  // Build the trait implementation
  impl_redis_orm(&ast)
}

fn impl_redis_orm(ast: &syn::DeriveInput) -> TokenStream {
  let name = &ast.ident;
  let gen = quote! {
    impl ActiveRecord for #name {
      fn db_prefix() -> String {
        // TODO: Convert name to lowercase separated with underscore.
        stringify!(#name).to_ascii_lowercase()
      }

      fn db_id(&self) -> Option<i64> {
        self.id
      }

      fn db_set_id(&mut self, id: Option<i64>) {
        self.id = id;
      }
    }

    impl StringMap for #name {}
  };
  gen.into()
}
